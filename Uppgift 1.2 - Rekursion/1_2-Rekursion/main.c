#include <stdio.h>
#include <assert.h>

int sum(int digit);

int main()
{
    assert(sum(1) == 1);
    assert(sum(2) == 3);
    assert(sum(3) == 6);
    assert(sum(4) == 10);
    assert(sum(5) == 15);
    assert(sum(20) == 210);
    assert(sum(0) == 0);
    assert(sum(-1) == 0);
    return 0;
}

int sum(int digit){

    printf("Sum(%d) is called\n", digit);

    if(digit >= 0){
        int x = digit + sum(digit - 1);
        printf("Sum(%d) returns %d\n", digit, x);
        return x;
    }
    else{
        printf("Sum(%d) returns 0\n", digit);
        return 0;
    }
}
