TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    list.c \
    Queue.c \
    set.c \
    stack.c \
    test_queue_stack_set.c

HEADERS += \
    list.h \
    Queue.h \
    Set.h \
    Stack.h
