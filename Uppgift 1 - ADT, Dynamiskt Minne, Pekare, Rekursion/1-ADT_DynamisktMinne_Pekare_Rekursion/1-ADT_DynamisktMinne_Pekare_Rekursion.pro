TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    dstring.c \
    dstring_test.c

HEADERS += \
    dstring.h
