#include "dstring.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

DString dstring_initialize(const char* str)
{
    assert(str != NULL);

    DString strReturn = (char*) calloc(strlen(str) + 1, sizeof (char));
    assert(strReturn != NULL);

    strcpy(strReturn, str);

    return strReturn;
}

int dstring_concatenate(DString* destination, DString source)
{
    assert(destination != NULL && *destination != NULL && source != NULL);

    DString tmp = (char*) realloc(*destination, strlen (*destination) + strlen (source) + 1);
    assert(tmp != NULL);
    *destination = tmp;

    strcat(*destination, source);

    return 1;
}

void dstring_truncate(DString* destination, unsigned int truncatedLength)
{
    assert(destination != NULL && truncatedLength > 0);

    DString tmp = (char*) realloc(*destination, truncatedLength * sizeof (char) + 1);
    if(tmp != NULL){
        *destination = tmp;
        *(*destination + truncatedLength) = '\0';
    }
}

void dstring_print(DString str, FILE* textfile)
{
    assert(textfile != NULL);

    fputs(str, textfile);
}

void dstring_delete(DString* stringToDelete)
{
    assert(stringToDelete != NULL);

    free(*stringToDelete);
    *stringToDelete = NULL;
}
