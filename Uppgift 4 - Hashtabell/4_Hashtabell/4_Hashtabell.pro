TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    HashTable.c \
    Person.c \
    test_HashTable.c

HEADERS += \
    Bucket.h \
    HashTable.h \
    Person.h
