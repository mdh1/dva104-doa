TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    BSTree.c \
    test_BSTree.c

HEADERS += \
    BSTree.h
